/* File : CrossVal.i */
%module CrossVal

%{
#include "CrossVal.h"
extern int CrossValidation(int tamano, int fold, float val, float train);
/*extern void txt(int vect[100]);*/
%}

%include "CrossVal.h"
extern int CrossValidation(int tamano, int fold, float val, float train);
/*extern void txt(int vect[100]);*/
