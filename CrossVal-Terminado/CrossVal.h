#ifndef CROSSVAL_H_INCLUDED
#define CROSSVAL_H_INCLUDED
#include <iostream>
#include <stdio.h>
#include <stdlib.h> //En TXT
#include <time.h>  //Para las semillas de generación aleatoria
#include <fstream> //Libreria para los TXT


int CrossValidation(int tamano, int fold, float val, float train);
//void txt(int vect[100]);

#endif // CROSSVAL_H_INCLUDED


